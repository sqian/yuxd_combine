#! /bin/bash

muonenergy="1GeV"
sigtype="maxwell_ci"
DMmass="10"

mypath=$PWD
echo $mypath

echo "UNSCALED Histograms"

echo signal velocity: ${sigtype} and muon energy: ${muonenergy}
if [ -d "UL_${sigtype}_muon${muonenergy}" ]; then
  rm -rf UL_${sigtype}_muon${muonenergy}
fi

mkdir UL_${sigtype}_muon${muonenergy}
cd UL_${sigtype}_muon${muonenergy}
echo Dark Matter mass ${DMmass}
cp -r ../datacard.txt datacard_${sigtype}_DM${DMmass}_muon${muonenergy}.txt
sed -i "s#ROOTFILE#${CI_PROJECT_DIR}/Hist_muon${muonenergy}.root#g" datacard_${sigtype}_DM${DMmass}_muon${muonenergy}.txt
sed -i "s/SIG/sig_${DMmass}/g" datacard_${sigtype}_DM${DMmass}_muon${muonenergy}.txt
sed -i "s/BKG/bkg/g" datacard_${sigtype}_DM${DMmass}_muon${muonenergy}.txt
combine -M AsymptoticLimits datacard_${sigtype}_DM${DMmass}_muon${muonenergy}.txt -n ${sigtype}_DM${DMmass}_muon${muonenergy} --run blind
cd $mypath

echo "WITH SCALED Histograms"

# Create scaled histograms
root -l -b -q scaleHist.C

if [ -d "UL_${sigtype}_muon${muonenergy}_scaled" ]; then
  rm -rf UL_${sigtype}_muon${muonenergy}_scaled
fi

mkdir UL_${sigtype}_muon${muonenergy}_scaled
cd UL_${sigtype}_muon${muonenergy}_scaled
echo Dark Matter mass ${DMmass}
cp -r ../datacard.txt datacard_${sigtype}_DM${DMmass}_muon${muonenergy}_scaled.txt
sed -i "s#ROOTFILE#${CI_PROJECT_DIR}/Hist_muon${muonenergy}_scaled.root#g" datacard_${sigtype}_DM${DMmass}_muon${muonenergy}_scaled.txt
sed -i "s/SIG/sig_${DMmass}/g" datacard_${sigtype}_DM${DMmass}_muon${muonenergy}_scaled.txt
sed -i "s/BKG/bkg/g" datacard_${sigtype}_DM${DMmass}_muon${muonenergy}_scaled.txt
combine -M AsymptoticLimits datacard_${sigtype}_DM${DMmass}_muon${muonenergy}_scaled.txt -n ${sigtype}_DM${DMmass}_muon${muonenergy}_scaled --run blind
cd $mypath

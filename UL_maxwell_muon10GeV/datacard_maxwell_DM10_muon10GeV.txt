imax 1
jmax 1
kmax *
---------------
shapes h_bkg * /home/pku/yuxd/bond/PKMUON_Beam_G4sim/Signal/analysis/root/Hist_muon10GeV.root hbkg
shapes h_sig * /home/pku/yuxd/bond/PKMUON_Beam_G4sim/Signal/analysis/root/Hist_muon10GeV.root hsig_10
shapes data_obs * /home/pku/yuxd/bond/PKMUON_Beam_G4sim/Signal/analysis/root/Hist_muon10GeV.root hbkg
---------------
bin bin1
observation -1
------------------------------
bin         bin1        bin1
process     h_sig       h_bkg
process     0           1
rate        -1          -1
------------------------------
bin1 autoMCStats 10 0 1
